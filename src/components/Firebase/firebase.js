import app from 'firebase/app';
import 'firebase/auth';

const config = {
    apiKey: 'AIzaSyDB-Z6WPyVobLiSq3c1hP6bZJQwKJ2CwTc',
    authDomain: 'test-task-qa.firebaseapp.com',
    databaseURL: 'https://test-task-qa.firebaseio.com',
    projectId: 'test-task-qa',
    storageBucket: '',
    messagingSenderId: '1097376792424',
    appId: '1:1097376792424:web:b7fb78720dc5256e',
};

class Firebase {
    constructor() {
        app.initializeApp(config);

        this.auth = app.auth();
    }

    doCreateUserWithEmailAndPassword = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();
}

export default Firebase;